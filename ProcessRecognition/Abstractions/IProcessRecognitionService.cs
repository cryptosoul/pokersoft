﻿using System;
using System.Collections.Generic;

namespace ProcessRecognition.Abstractions
{
    public interface IProcessRecognitionService
    {
        Dictionary<string, IntPtr> GetSimilarProcesses(string mathTitle);
        Dictionary<string, IntPtr> GetAllProcesses();

        IntPtr FindProcessByTitle(string title);

        string GetWindowTitle(IntPtr hWnd);
    }
}
