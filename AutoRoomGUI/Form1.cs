﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CvRecognition;
using PokerSeatBot.Abstractions;
using PokerSeatBot.Services;
using ProcessRecognition.Services;
using ScreenShotManagement.Services;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var recognitionService = new WinApiProcessRecognitionService();
            var screenCaptureService = new OpenGlScreenCaptureService(recognitionService);
            var chicoTableOpener = new ChicoTableOpener(screenCaptureService, recognitionService);
            PokerRoom = new ChicoRoomService(chicoTableOpener, screenCaptureService, recognitionService, new CvSearch());
        }

        private IPokerRoom PokerRoom { get; set; }

        private void Label3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private bool _isRunned;
        private void Label2_Click(object sender, EventArgs e)
        {
            _isRunned = !_isRunned;
            label2.Text = $@"Is enabled: {_isRunned}";
            if (_isRunned)
            {
                Task.Run(async () =>
                {
                    while (true)
                    {
                        if (!_isRunned)
                            return;

                        await TimerTick();
                        await Task.Delay(1500);
                    }
                });
            }
        }

        private async Task TimerTick()
        {
            Directory.CreateDirectory("Screens");
            try
            {
                await PokerRoom.OpenTables(10);
                PokerRoom.Scan();

                var info = PokerRoom.Tables.FirstOrDefault()?.Places;
                //label1.Text = info != null ? info.ToJson() : "MVP";
            }
            catch (Exception exception)
            {
                label1.Text = exception.Message;
            }
        }

        private void Label4_Click(object sender, EventArgs e)
        {
            PokerRoom.OpenTables();
        }
    }
}
