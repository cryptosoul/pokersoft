﻿namespace CvRecognition.Models
{
    public class Place
    {
        public Point Cord { get; set; }
        public PlaceType Type { get; set; }
    }
}
