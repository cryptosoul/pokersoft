﻿using System;

namespace CvRecognition.Models
{
    public class PokerTable
    {
        public string Title { get; set; }
        public IntPtr WindowPointer { get; set; }

        public string ImagePath { get; set; }
        public Place[] Places { get; set; }
    }
}
