﻿namespace CvRecognition.Models
{
    public enum PlaceType
    {
        FreePlace,
        Unknown,
        Red,
        Green,
        Blue,
        Yellow,
        Orange,
        Pink,
        TableCord
    }
}
