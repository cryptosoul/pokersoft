﻿using System.Text;

namespace CvRecognition.Extensions
{
    public static class StringExtensions
    {
        public static byte[] ToByteArray(this string message)
        {
            return Encoding.UTF8.GetBytes(message);
        }
    }
}
