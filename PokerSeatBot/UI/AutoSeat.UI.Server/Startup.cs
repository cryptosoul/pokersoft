using System.Diagnostics;
using System.IO;
using AutoSeat.Core.Configs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AutoSeat.UI.Server.Data;
using ProcessRecognition.Abstractions;
using ProcessRecognition.Services;
using ScreenShotManagement.Abstractions;
using ScreenShotManagement.Services;
using PokerSeatBot.Abstractions;
using PokerSeatBot.Services;
using CvRecognition.Services;

namespace AutoSeat.UI.Server
{
    public class Startup
    {
        private readonly AppConfig _appConfig;
        public Startup(IHostEnvironment environment)
        {
            _appConfig = AppConfig.Read();
            ServersPool.AddServices(1);
            var path = !environment.IsDevelopment()
                ? Path.GetFullPath("Host/AutoSeat.UI.Host.exe")
                : Path.GetFullPath("bin/Debug/netcoreapp3.0/Host/AutoSeat.UI.Host.exe");
            Process.Start(path);
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<CounterService>();

            services.AddSingleton<CvSearch>();

            services.AddSingleton<IProcessRecognitionService, WinApiProcessRecognitionService>();
            services.AddSingleton<IScreenCaptureService, OpenGlScreenCaptureService>();

            services.AddSingleton<ITableOpener, ChicoTableOpener>();
            services.AddSingleton<IPokerRoom, ChicoRoomService>();

            services.AddSingleton(_appConfig);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
