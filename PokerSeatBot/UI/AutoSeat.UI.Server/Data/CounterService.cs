﻿using PokerSeatBot.Abstractions;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSeat.UI.Server.Data
{
    public class CounterService
    {
        public CounterService(IPokerRoom pokerRoom)
        {
            _pokerRoom = pokerRoom;
        }

        private readonly IPokerRoom _pokerRoom;

        public async Task OpenTables()
        {
            var isOpened = await _pokerRoom.OpenTables();
            if (isOpened)
                await _pokerRoom.ScanForNewTables();
        }

        public bool IsRunned;
        public void StartScanning()
        {
            //_isRunned = true;
            if (!IsRunned) return;
            _pokerRoom.OpenedTables.Clear();
            Directory.CreateDirectory("Screens");
            Task.Run(async () =>
            {
                while (IsRunned)
                {
                    await TimerTick();
                    await Task.Delay(1500);
                }
            });
        }

        private async Task TimerTick()
        {
            try
            {
                await _pokerRoom.ScanForNewTables();
                await _pokerRoom.Scan();

                var info = _pokerRoom.TargetTables.FirstOrDefault()?.Places;
            }
            catch (Exception)
            {
                //
            }
        }
    }
}
