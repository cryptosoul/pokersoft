﻿using CefSharp;
using CefSharp.WinForms;
using System.Windows.Forms;

namespace AutoSeat.UI.Host
{
    public partial class Form1 : Form
    {
        ChromiumWebBrowser browser = null;
        public Form1()
        {
            Cef.Initialize(new CefSettings());
            InitializeComponent();
            browser = new ChromiumWebBrowser("http://localhost:5000");
            //browser.Load(@"C:\Users\Admin\Desktop\MainScreen\index.html");
            Controls.Add(browser);
            browser.Dock = DockStyle.Fill;
        }
    }
}
