﻿using PokerSeatBot.Abstractions;
using ProcessRecognition.Abstractions;
using ScreenShotManagement.Abstractions;
using System.Linq;
using System.Threading;
using CvRecognition.Services;
using WinApi;

namespace PokerSeatBot.Services
{
    public class ChicoTableOpener : ITableOpener
    {
        private readonly IScreenCaptureService _screenCaptureService;
        private readonly IProcessRecognitionService _processRecognitionService;

        public ChicoTableOpener(IScreenCaptureService screenCaptureService, IProcessRecognitionService processRecognitionService)
        {
            _screenCaptureService = screenCaptureService;
            _processRecognitionService = processRecognitionService;
        }

        private const string LobbyTitle = "TigerGaming Lobby";
        private const string ImgPath = "Lobby.png";
        public bool OpenTables()
        {
            var windowPair = _processRecognitionService
                .GetSimilarProcesses(LobbyTitle)
                .FirstOrDefault();
            if (windowPair.Key == null)
                return false;

            var window = windowPair.Value;
            var img = _screenCaptureService.CaptureWindow(window);
            img?.Save(ImgPath);

            var pipe = ServersPool.Next();
            var tablesPos = pipe.Send(new InputArgs { Type = "Lobby", Value = ImgPath });

            foreach (var pos in tablesPos)
            {
                ClickerService.ClickOnPoint(window, pos.Cord);
                ClickerService.ClickOnPoint(window, pos.Cord);
                Thread.Sleep(2000);
                User32.MoveWindow(window, 0,0,500, 385, true);
            }
            return true;
        }
    }
}
