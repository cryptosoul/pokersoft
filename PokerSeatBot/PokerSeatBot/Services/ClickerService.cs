﻿using System;
using System.Runtime.InteropServices;
using CvRecognition.Models;
using ScreenShotManagement;
using WinApi;

namespace PokerSeatBot.Services
{
    public class ClickerService
    {
        public static void ActivateWindow(IntPtr wndHandle)
        {
            User32.SetForegroundWindow(wndHandle);
        }

        public static void ClickOnPoint(IntPtr wndHandle, Point point)
        {
            var pos = User32.GetCursorPosition();
            ActivateWindow(wndHandle);
            var clientPoint = new System.Drawing.Point(point.X, point.Y - 30);
            User32.ClientToScreen(wndHandle, ref clientPoint);
            User32.SetCursorPos(clientPoint.X, clientPoint.Y);

            var inputMouseDown = new User32.INPUT {Type = 0};
            inputMouseDown.Data.Mouse.Flags = 0x0002; // left button down

            var inputMouseUp = new User32.INPUT {Type = 0};
            inputMouseUp.Data.Mouse.Flags = 0x0004; // left button up

            var inputs = new[] { inputMouseDown, inputMouseUp };
            User32.SendInput((uint)inputs.Length, inputs, Marshal.SizeOf(typeof(User32.INPUT)));
            User32.SetCursorPos(pos.X, pos.Y);
        }
    }
}
