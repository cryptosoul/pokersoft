﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoSeat.Core.Configs;
using CvRecognition.Models;
using CvRecognition.Services;
using PokerSeatBot.Abstractions;
using ProcessRecognition.Abstractions;
using ScreenShotManagement.Abstractions;
using WinApi;

namespace PokerSeatBot.Services
{
    public abstract class RoomServiceBase : IPokerRoom
    {
        public List<PokerTable> TargetTables { get; set; } = new List<PokerTable>();
        public List<PokerTable> OpenedTables { get; set; } = new List<PokerTable>();

        public abstract Task<bool> OpenTables();

        public abstract Task Scan(bool autoSeat = true);

        public abstract Task ScanForNewTables();

        public abstract Task Seat(PokerTable pokerTable);
    }

    public class ChicoRoomService : RoomServiceBase
    {
        private readonly AppConfig _appConfig;

        public ChicoRoomService(ITableOpener tableOpener, IScreenCaptureService screenCaptureService, IProcessRecognitionService recognitionService, CvSearch cvSearch, AppConfig appConfig)
        {
            _appConfig = appConfig;
            TableOpener = tableOpener;
            ScreenCaptureService = screenCaptureService;
            RecognitionService = recognitionService;
            CvSearch = cvSearch;
        }

        public ITableOpener TableOpener { get; }
        public IScreenCaptureService ScreenCaptureService { get; private set; }

        public IProcessRecognitionService RecognitionService { get; private set; }
        public CvSearch CvSearch { get; }

        public override Task<bool> OpenTables()
        {
            return Task.FromResult(TableOpener.OpenTables());
        }

        public override Task ScanForNewTables()
        {
            try
            {
                var processes = RecognitionService.GetSimilarProcesses("$");
                var lobby = processes.FirstOrDefault(pair => pair.Key.Contains("TigerGaming Lobby"));
                if (!lobby.Equals(default(KeyValuePair<string, IntPtr>)))
                    processes.Remove(lobby.Key); // remove lobby window from dictionary

                var allTables = processes
                    .Where(pair => !pair.Equals(lobby))
                    .Select(p => new PokerTable
                    {
                        Title = p.Key,
                        WindowPointer = p.Value,
                        ImagePath = $"Screens/{p.Value}.png"
                    })
                    .ToArray();

                TargetTables.AddRange(allTables.Except(OpenedTables).Except(TargetTables).ToArray());

                var deleteList = TargetTables.Except(allTables);
                foreach (var table in deleteList)
                {
                    TargetTables.Remove(table);    
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }

            return Task.CompletedTask;
        }


        public override async Task Scan(bool autoSeat = true)
        {
            foreach(var pokerTable in TargetTables)
            {
                try
                {
                    ScreenCaptureService.SetWindowSize(pokerTable.WindowPointer, 500, 385);
                    var image = ScreenCaptureService.CaptureWindow(pokerTable.WindowPointer);
                    image.Save(pokerTable.ImagePath);

                    pokerTable.Places = CvSearch.Search(pokerTable.ImagePath);
                    if (autoSeat && pokerTable.Places != null && pokerTable.Places.Any(place => place.Type == PlaceType.FreePlace))
                    {
                        if (pokerTable.Places.Count(place => _appConfig.FishNoteColors[place.Type] ) > 0)
                        {
                            await Seat(pokerTable);
                            TargetTables.Remove(pokerTable);
                            OpenedTables.Add(pokerTable);
                            await Task.Delay(500);
                            User32.MoveWindow(pokerTable.WindowPointer, 800, 400, 800, 600, true);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }
            }
        }

        public override Task Seat(PokerTable pokerTable)
        {
            Debug.WriteLine($"Try to seat into {pokerTable.Title} table");
            var items = pokerTable.Places
                .Zip(Enumerable.Range(0, pokerTable.Places.Length), (place, index) => new { place, index })
                .ToArray();

            var freePlace = items
                .Where(arg => arg.place.Type != PlaceType.Red)
                .Select(place =>
                {
                    var freePlaceIndex = items.Skip(place.index).FirstOrDefault(place1 => place1.place.Type == PlaceType.FreePlace)?.place;
                    return new { place.place, place.index, freePlaceIndex };
                })
                .LastOrDefault(arg => arg.freePlaceIndex != null);
            if (freePlace != null)
                ClickerService.ClickOnPoint(pokerTable.WindowPointer, freePlace.freePlaceIndex.Cord);

            return Task.CompletedTask;
        }
    }
}
