﻿namespace PokerSeatBot.Abstractions
{
    public interface ITableOpener
    {
        bool OpenTables();
    }
}
