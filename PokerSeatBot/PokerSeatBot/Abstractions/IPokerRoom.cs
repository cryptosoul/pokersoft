﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CvRecognition.Models;

namespace PokerSeatBot.Abstractions
{
    public interface IPokerRoom
    {
        List<PokerTable> TargetTables { get; set; }
        List<PokerTable> OpenedTables { get; set; }

        Task<bool> OpenTables();
        Task ScanForNewTables();

        Task Scan(bool autoSeat = true);
        Task Seat(PokerTable pokerTable);
    }
}
