﻿using System.Collections.Generic;
using System.IO;
using CvRecognition.Models;
using Symod.Libs.Common.Extensions;

namespace AutoSeat.Core.Configs
{
    public class AppConfig
    {
        public Dictionary<PlaceType, bool> FishNoteColors = new Dictionary<PlaceType, bool>
        {
            { PlaceType.Red, false },
            { PlaceType.Green, false },
            { PlaceType.Blue, false },
            { PlaceType.Orange, false },
            { PlaceType.Pink, false },
            { PlaceType.Yellow, false },
            { PlaceType.Unknown, false },
            { PlaceType.FreePlace, false },
        };

        public static void Save(AppConfig config)
        {
            File.WriteAllText("AppConfig.json", config.ToJson());
        }

        public static AppConfig Read()
        {
            if (File.Exists("AppConfig.json"))
            {
                return File.ReadAllText("AppConfig.json").Deserialize<AppConfig>();
            }
            else
            {
                var appConfig = new AppConfig();
                Save(appConfig);
                return appConfig;
            }
        }
    }
}
