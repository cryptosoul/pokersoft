﻿import cv2
import numpy as np
import math

TEMP_NOTE = [[210, 78], [387, 122], [387, 240], [210, 280], [33, 240], [33, 122]]
TEMP_PLAYER = cv2.imread("n1.png", cv2.IMREAD_GRAYSCALE)
TEMP_PLACE = cv2.imread("n2.png", cv2.IMREAD_GRAYSCALE)

TARGET_COLORS = {"Red": (240, 42, 42), "Orange": (200, 105, 0),
                 "Green": (0, 255, 0), "Yellow": (200, 140, 10),
                 "Blue": (0, 0, 255), "Pink": (255, 0, 128),
                 "Violet": (128, 0, 255), "Unknown": (74, 74, 74)}
def comprehension(player, notes):
    note = []
    for i in notes:
        note.append(i[0])
    return [x for x in player if x not in note]

def convert_cord(Positions, type):
    cord = []
    for Position in Positions:
        cord.append({"Cord": {"x": Position[0], "y": Position[1]}, "Type": type})
    return cord

def sorting_cord(points, origin):
    cord = []
    for i in points:
        # y = i["Cord"]["y"]
        # x = i["Cord"]["x"]
        A = int(math.atan2((origin[1] // 2) - i["Cord"]["y"], (origin[0] // 2) - i["Cord"]["x"]) / math.pi * 180)
        if A < 0:
            A += 360
        cord.append(A)
    z = [x for _, x in sorted(zip(cord, points))]
    return z

def place(image, temp=TEMP_PLACE, limit=0.8):
    cord = []
    w, h = temp.shape[::-1]
    result = cv2.matchTemplate(image, temp, cv2.TM_CCOEFF_NORMED)
    loc = np.where(result >= limit)
    for point in zip(*loc[::-1]):
        x, y = point[0] + (w // 2), point[1] + (h // 2)
        cord.append([x, y])
    return cord

def finde_note(image, temp, w, h, limit=0.77):
    cord = []
    result = cv2.matchTemplate(image, temp, cv2.TM_CCOEFF_NORMED)
    loc = np.where(result >= limit)
    for point in zip(*loc[::-1]):
        x, y = point[0], point[1]
        cord.append([x, y])
    return cord

def position_range(notes, positions):
    cord = []
    for note in notes:
        position_cord = []
        for position in positions:
            position_cord.append(int(math.hypot(position[0] - note[0], position[1] - note[1])))
        #print(position_cord)
        val, idx = min((val, idx) for (idx, val) in enumerate(position_cord))
        #print(position_cord, val, idx)
        if val <= 55:
            cord.append([positions[idx], note])

    return cord

def dominant_color(image, CordNotes, w, h):
    cord = []
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    for note in CordNotes:
        note_cord = note[1]
        img = image[note_cord[1]:note_cord[1] + h, note_cord[0]:note_cord[0] + w]  # [y:y+h, x:x+w]

        pixels = np.float32(img.reshape(-1, 3))

        n_colors = 10
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 200, .1)
        flags = cv2.KMEANS_RANDOM_CENTERS

        _, labels, palette = cv2.kmeans(pixels, n_colors, None, criteria, 10, flags)
        _, counts = np.unique(labels, return_counts=True)

        def color_difference(color1, color2):
            a = sum([abs(component1 - component2) for component1, component2 in zip(color1, color2)])
            return a

        dominant = max(np.int32(palette).tolist())

        differences = [[color_difference(dominant, target_value), target_name] for target_name, target_value in
                       TARGET_COLORS.items()]
        differences.sort()  # sorted by the first element of inner lists
        color = differences[0][1]
        cord.append({"Cord": {"x": note[0][0], "y": note[0][1]}, "type": color})
    return cord

def search(path):
    image = cv2.imread(path)
    np_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    w, h = 2, 22
    free_place = place(np_image, TEMP_PLACE)
    player = place(np_image, TEMP_PLAYER)
    note = position_range(TEMP_NOTE, player)
    AllPosition = dominant_color(image, note, w, h)
    c_place = convert_cord(free_place, "freePlace")
    AllPosition += c_place
    return sorting_cord(AllPosition, (392, 516))