import cv2
import numpy as np

TEMP_NOTE = cv2.imread("1.png", cv2.IMREAD_GRAYSCALE)
TEMP_NOTE1 = cv2.imread("2.png", cv2.IMREAD_GRAYSCALE)

def place(image, temp, limit=0.8):
    cord = []
    w, h = temp.shape[::-1]
    result = cv2.matchTemplate(image, temp, cv2.TM_CCOEFF_NORMED)
    loc = np.where(result >= limit)
    for point in zip(*loc[::-1]):
        x, y = point[0] + (w // 2), point[1] + (h // 2)
        cord.append([x, y])
    return cord

def convert_cord(Positions, type):
    cord = []
    for Position in Positions:
        cord.append({"Cord": {"x": Position[0], "y": Position[1]}, "Type": type})
    return cord

def search(path):
    image = cv2.imread(path)
    np_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    a = place(np_image, TEMP_NOTE)
    s = place(np_image, TEMP_NOTE1)
    note = convert_cord((a+s), "TableCord")
    return note

