﻿using System;

namespace CvRecognition.Models
{
    public class PokerTable
    {
        public string Title { get; set; }
        public IntPtr WindowPointer { get; set; }

        public string ImagePath { get; set; }
        public Place[] Places { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            var p1 = obj as PokerTable;
            return p1.WindowPointer == WindowPointer;
        }

        public override int GetHashCode()
        {
            return WindowPointer.GetHashCode();
        }
    }
}
