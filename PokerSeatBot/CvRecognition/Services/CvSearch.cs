﻿using System;
using System.Diagnostics;
using CvRecognition.Models;

namespace CvRecognition.Services
{
    public class CvSearch
    {
        private PipeService PipeService => ServersPool.Next();

        public Place[] Search(string imagePath)
        {
            var pipe = PipeService;
            try
            {
                var response = pipe.Send(new InputArgs { Type = "Table", Value = imagePath });
                return response;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return null;
            }
        }
    }
}
