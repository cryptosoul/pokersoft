﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace CvRecognition.Services
{
    public static class ServersPool
    {
        private const string BasePipeName = "OpenCv_PipeServer_";
        public static List<PipeService> Services { get; set; } = new List<PipeService>();
        public static IEnumerator PipesEnumerator { get; set; }
        public static PipeService Next()
        {
            if (PipesEnumerator.MoveNext())
                return (PipeService)PipesEnumerator.Current;

            PipesEnumerator.Reset();
            return Next();
        }

        public static void Close(PipeService pipeService)
        {
            Services.Remove(pipeService);
            PipesEnumerator = Services.GetEnumerator();
        } 

        public static void AddServices(int count)
        {
            var startIndex = Services.Count;
            count = Services.Count + count;
            for (var i = startIndex; i < count; i++)
            {
                Services.Add(new PipeService(BasePipeName + i));
                Thread.Sleep(500);
            }

            PipesEnumerator = Services.GetEnumerator();
        }
    }
}
