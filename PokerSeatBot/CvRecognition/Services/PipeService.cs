﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using CvRecognition.Extensions;
using CvRecognition.Models;
using Symod.Libs.Common.Extensions;

namespace CvRecognition.Services
{
    public class PipeService
    {
        private readonly string _pipeName;
        private Process _process;

        public PipeService(string pipeName)
        {
            _pipeName = pipeName;
            StartServer();
        }

        ~PipeService()
        {
            CloseServer();
        }

        public Place[] Send(InputArgs message)
        {
            using (var pipeClient = new NamedPipeClientStream(".", _pipeName, PipeDirection.InOut))
            {
                pipeClient.Connect();
                var bytes = message.ToJson().ToByteArray();
                pipeClient.Write(bytes, 0, bytes.Length);

                using (var sr = new StreamReader(pipeClient))
                {
                    var responseStr = "";
                    string temp;
                    while ((temp = sr.ReadLine()) != null)
                    {
                        responseStr += temp;
                    }

                    responseStr = responseStr.Replace("\"", "").Replace("'", "\"");
                    var response = responseStr.Deserialize<Place[]>(); // response
                    var red = response?.Count(place => place.Type == PlaceType.Red);
                    var green = response?.Count(place => place.Type == PlaceType.Green);
                    var free = response?.Count(place => place.Type == PlaceType.FreePlace);
                    var unknowns = response?.Count(place => place.Type == PlaceType.Unknown);
                    Debug.WriteLine($"Free: {free} | Red: {red} | Green {green} | Unknown {unknowns}");
                    return response;
                }
            }
        }

        public void StartServer()
        {
            try
            {
                _process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "cmd.exe",
                        Arguments = "/K py Server.py " + _pipeName,
                        //UseShellExecute = true,
                        RedirectStandardOutput = true,
                        RedirectStandardInput = true,
                        CreateNoWindow = true
                    }
                };
                _process.Start();
                AppDomain.CurrentDomain.ProcessExit += (sender, args) => CloseServer();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
        }

        public void CloseServer()
        {
            try
            {
                _process.CloseMainWindow();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
        }
    }

    public class InputArgs
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
