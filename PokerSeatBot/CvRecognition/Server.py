﻿import sys
import time
import sys
import win32pipe, win32file, win32event, winerror, pywintypes

import json
import Search
import Launcher

class Input(object):
     def __init__(self, j):
         self.__dict__ = json.loads(j)
         self.type = self.__dict__['Type']
         self.imagePath = self.__dict__['Value']

def pipe_server():
    name = r'\\.\pipe\%s' % (sys.argv[1])
    pipe = win32pipe.CreateNamedPipe(
        name,
        win32pipe.PIPE_ACCESS_DUPLEX,
        win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_READMODE_MESSAGE | win32pipe.PIPE_WAIT,
        100, 65536, 65599,
        0,
        None)
    
    try:
        print("Connecting to pype: " + name)
        win32pipe.ConnectNamedPipe(pipe, None)
        print("Connected to pype: " + name)
        path, paramsStr = win32file.ReadFile(pipe, 2048, None)  
        while path == winerror.ERROR_MORE_DATA:            
            path, data = win32file.ReadFile(pipe, 2048, None)
            paramsStr += data   

        paramsStr = str(paramsStr, 'utf-8')
        params = Input(paramsStr)
        if params.type == "Lobby":
            print('Lobby scan')
            bResults = Launcher.search(params.imagePath)
        else:
            print('Table scan')
            bResults = Search.search(params.imagePath)
        results = str.encode(f"{bResults}")
        win32file.WriteFile(pipe, results)
    except:
        print("error")
    finally:
        win32file.CloseHandle(pipe)
	
if __name__ == '__main__':
    print('Server is started')
    while True:
        pipe_server()
        