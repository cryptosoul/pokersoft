﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using ProcessRecognition.Abstractions;
using WinApi;

namespace ProcessRecognition.Services
{
    public class WinApiProcessRecognitionService : IProcessRecognitionService
    {
        public Dictionary<string, IntPtr> GetAllProcesses()
        {
            return GetDesktopWindowsTitles();
        }

        public Dictionary<string, IntPtr> GetSimilarProcesses(string matchTitle)
        {
            var allProcesses = GetAllProcesses();
            var similarProcesses = allProcesses
                .Where(pair => pair.Key.ToLowerInvariant().Contains(matchTitle.ToLowerInvariant()));

            return similarProcesses.ToDictionary(x=>x.Key, x=>x.Value);
        }

        public IntPtr FindProcessByTitle(string title)
        {
            var allProcesses = GetAllProcesses();
            return allProcesses[title];
        }
        public string GetWindowTitle(IntPtr hWnd)
        {
            return GetWindowText(hWnd);
        }

        /// <summary>
        /// Return the window title of handle
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        public static string GetWindowText(IntPtr hWnd)
        {
            var strbTitle = new StringBuilder(User32.MaxTitle);
            var nLength = User32.GetWindowText(hWnd, strbTitle, strbTitle.Capacity + 1);
            strbTitle.Length = nLength;
            return strbTitle.ToString();
        }

        /// <summary>
        /// Return titles of all visible windows on desktop
        /// </summary>
        /// <returns>List of titles in type of string</returns>
        private static Dictionary<string, IntPtr> GetDesktopWindowsTitles()
        {
            var lstTitles = new Dictionary<string, IntPtr>();

            var selector = new User32.EnumDelegate((wnd, param) =>
            {
                var strTitle = GetWindowText(wnd);
                if (strTitle != "" & User32.IsWindowVisible(wnd)) //
                {
                    try
                    {
                        if (lstTitles.ContainsKey(strTitle))
                        {
                            return true;
                        }
                        lstTitles.Add(strTitle, wnd);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                        throw;
                    }
                }
                return true;
            });

            var bSuccessful = User32.EnumDesktopWindows(IntPtr.Zero, selector, IntPtr.Zero); //for current desktop
            if (bSuccessful)
            {
                return lstTitles;
            }
            else
            {
                // Get the last Win32 error code
                var nErrorCode = Marshal.GetLastWin32Error();
                var strErrMsg = $"EnumDesktopWindows failed with code {nErrorCode}.";
                throw new Exception(strErrMsg);
            }
        }
    }
}