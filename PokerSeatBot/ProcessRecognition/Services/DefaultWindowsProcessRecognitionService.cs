﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ProcessRecognition.Abstractions;

namespace ProcessRecognition.Services
{
    public class DefaultWindowsProcessRecognitionService : IProcessRecognitionService
    {
        public Dictionary<string, IntPtr> GetSimilarProcesses(string mathTitle)
        {
            return GetAllProcesses();
        }

        public Dictionary<string, IntPtr> GetAllProcesses()
        {
            return Process
                .GetProcesses()
                .ToDictionary(process => process.MainWindowTitle, process => process.MainWindowHandle);
        }

        public IntPtr FindProcessByTitle(string title)
        {
            return GetAllProcesses()
                .Single(process => process.Key.ToLower().Contains(title.ToLower()))
                .Value;
        }

        public string GetWindowTitle(IntPtr hWnd)
        {
            throw new NotImplementedException();
        }
    }
}
