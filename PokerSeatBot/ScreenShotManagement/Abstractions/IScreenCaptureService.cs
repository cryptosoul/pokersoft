﻿using System;
using System.Drawing;

namespace ScreenShotManagement.Abstractions
{
    public interface IScreenCaptureService
    {
        Image CaptureSimilarWindow(string processName);
        Image CaptureWindow(string title);
        Image CaptureWindow(IntPtr title);
        void SetWindowSize(IntPtr handle, int width, int height);
    }
}
