﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using ProcessRecognition.Abstractions;
using ScreenShotManagement.Abstractions;
using WinApi;

namespace ScreenShotManagement.Services
{
    public class OpenGlScreenCaptureService : IScreenCaptureService
    {
        private readonly IProcessRecognitionService _processRecognitionService;

        public OpenGlScreenCaptureService(IProcessRecognitionService processRecognitionService)
        {
            _processRecognitionService = processRecognitionService;
        }

        public Image CaptureWindow(string title)
        {
            var process = _processRecognitionService.FindProcessByTitle(title);

            return CaptureWindow(process);
        }

        public void SetWindowSize(IntPtr handle, int width, int height)
        {
            User32.MoveWindow(handle, 0, 0, width, height, true);
        }

        public Image CaptureWindow(IntPtr handle)
        {
            try
            {
                // get te hDC of the target window
                var hdcSrc = User32.GetWindowDC(handle);
                // get the size
                var windowRect = User32.GetWindowSize(handle);
                var width = windowRect.right - windowRect.left;
                var height = windowRect.bottom - windowRect.top;
                // create a device context we can copy to
                var hdcDest = GDI32.CreateCompatibleDC(hdcSrc);
                // create a bitmap we can copy it to,
                // using GetDeviceCaps to get the width/height
                var hBitmap = GDI32.CreateCompatibleBitmap(hdcSrc, width, height);
                // select the bitmap object
                var hOld = GDI32.SelectObject(hdcDest, hBitmap);
                // bitblt over
                GDI32.BitBlt(hdcDest, 0, 0, width, height, hdcSrc, 0, 0, GDI32.SRCCOPY);
                // restore selection
                GDI32.SelectObject(hdcDest, hOld);
                // clean up 
                GDI32.DeleteDC(hdcDest);
                User32.ReleaseDC(handle, hdcSrc);
                // get a .NET image object for it
                Image img = Image.FromHbitmap(hBitmap);
                // free up the Bitmap object
                GDI32.DeleteObject(hBitmap);
                return img;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return new Bitmap(10,10);
            }
        }

        public Image CaptureSimilarWindow(string processName)
        {
            var processPtr = _processRecognitionService
                .GetSimilarProcesses(processName)
                .FirstOrDefault()
                .Value;

            return CaptureWindow(processPtr);
        }
    }
}
